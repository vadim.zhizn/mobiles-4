using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Camera cam;

    private float maxWidth;

    public GameObject ball;
    public GameObject bomb;
    public Text timerText;
    public Text message;
    public Button btn;


    public float timeLeft;
    private float startTime;

    void Start()
    {
        if (cam == null)
            cam = Camera.main;
        btn.gameObject.SetActive(false);
        Vector3 upperCorner = new Vector3(Screen.width, Screen.height, 0f);
        Vector3 targetWidth = cam.ScreenToWorldPoint(upperCorner);

        float ballWidth = ball.GetComponent<Renderer>().bounds.extents.x;
        maxWidth = targetWidth.x - ballWidth;
        startTime = timeLeft;
        StartCoroutine(Spawn());
        UpdateText();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft < 0 || timeLeft == 0)
        {
            timeLeft = 0;
            btn.gameObject.SetActive(true);
        }
        UpdateText();
    }

    public void SetTimeLeftNull()
    {
        timeLeft = 0;
        message.text = "You lose";
    }

    IEnumerator Spawn()
    {
        while (timeLeft > 0)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-maxWidth, maxWidth), transform.position.y + 10f, 0f);
            Quaternion spawnRotation = Quaternion.identity;

            if(Random.Range(0, 4) == 0)
                Instantiate(bomb, spawnPosition, spawnRotation);
            else
                Instantiate(ball, spawnPosition, spawnRotation);

            yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));
        }

    }
    void UpdateText()
    {
        timerText.text = $"Time Left: {Mathf.RoundToInt(timeLeft)}";
    }
    public void OnClickRestart()
    {
        timeLeft = startTime;
        btn.gameObject.SetActive(false);
        message.text = "";
        StartCoroutine(Spawn());
    }
}
