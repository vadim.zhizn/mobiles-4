using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour
{
    [SerializeField] private GameController _gameController;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(other.gameObject);
        if (other.tag == "Finish" && _gameController != null)
        {
            _gameController.SetTimeLeftNull();
        }
    }
}
